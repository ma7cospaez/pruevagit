*** Setting ***
Library     String
Library     SeleniumLibrary

*** Setting ***
Library     String
Library     SeleniumLibrary

*** Variables ***
${browser}      chrome
${homepage}     https://www.mercadolibre.com.ar/

*** Keywords ***
Open Homepage
    Open Browser   ${homepage}   ${browser}
*** Test Cases ***
C001 Hacer Clic en sub carpetas
    Open Homepage
    Set Global Variable   @{subcarpetas}  /html/body/header/div/div[2]/ul/li[3]/a   /html/body/header/div/div[2]/ul/li[4]/a   /html/body/header/div/div[2]/ul/li[5]/a   
	FOR    ${supcarpeta}   IN   @{subcarpetas}
	    Wait Until Element Is Visible    xpath=${supcarpeta}
	    Click Element      xpath=${supcarpeta}
	    Wait Until Element Is Visible      class=nav-menu-item
	    Click Element      xpath=/html/body/header/div/a[2]
    END
    
    Close Browser  